import numpy as np
import time
import cProfile

# Generar una lista aleatoria de 2000 elementos
random_list = list(np.random.randint(low=0, high=20000, size=2000))

# Imprime la lista generada
print(f"This is the list without organization:\n\n {random_list}\n\n")

def bubble_sort(arr):
    n = len(arr)
    for i in range(n):
        # Indicador de si se realizaron intercambios en esta pasada
        swapped = False
        # Últimos i elementos ya están en su lugar correcto
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1]:
                # Intercambiar si los elementos están en el orden incorrecto
                arr[j], arr[j+1] = arr[j+1], arr[j]
                swapped = True
        # Si no hubo intercambios en la pasada, la lista ya está ordenada
        if not swapped:
            break
    return arr

# Medir el tiempo de ejecución del bubble sort
start_time = time.time()
sorted_list = bubble_sort(random_list.copy())  # Usar una copia de la lista para no modificar la original
end_time = time.time()

# Mostrar la lista organizada y el tiempo de ejecución
print("Organized list\n\n")
print(sorted_list)
print(f"Execution time: {end_time - start_time} seconds")

# Perfil del rendimiento del bubble sort
cProfile.run('bubble_sort(random_list.copy())')
