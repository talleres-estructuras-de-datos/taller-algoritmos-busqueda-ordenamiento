import numpy as np

# Generar una lista aleatoria de 2000 elementos
random_list = list(np.random.randint(low=0, high=200, size=2000))

# Imprime la lista generada
print(random_list)

def linear_search(arr, x):
    iterations = []
    #Por cada elemento del rango de la lista ira a buscar 
    for i in range(len(arr)):
        #Suma por cada iteracion que haga en la lista y muestra el valor que hay en el index
        iterations.append(f"Iteration {i+1}: Checking index {i}, value {arr[i]}")
        #Si el valor a buscar es igual al del espacio en la lista se mostrara donde esta almacenado 
        if arr[i] == x:
            iterations.append(f"The value {x} is currently at index {i}")
            return iterations
    iterations.append(f"We couldn't find the value {x} in the array. Apologies")
    return iterations

# Solicitar al usuario el valor a buscar
needed_value = int(input("Please enter the value that you want to find in the array: "))

# Realizar la búsqueda lineal y almacenar todas las iteraciones
iterations = linear_search(random_list, needed_value)

# Imprimir todas las iteraciones
for iteration in iterations:
    print(iteration)
