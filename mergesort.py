import numpy as np
import time
import cProfile

# Generar una lista aleatoria de 2000 elementos
random_list = list(np.random.randint(low=0, high=20000, size=2000))

# Imprime la lista generada
print(f"This is the list without organization:\n\n {random_list}\n\n")

def mergesort(arr):
    #Si la lista tiene 0 o 1 elementos ya esta ordenada
    if len(arr) <= 1:
        return arr

    #Encuentra el punto medio de la lista
    mid = len(arr) // 2

    #Divide la lista en dos mitades para ordenarlas recursivamente
    left = mergesort(arr[:mid])
    right = mergesort(arr[mid:])

    #Se combina las mitades ordenadas
    return merge(left, right)

def merge(left, right):
    result = []  #Esta es la lista para almacenar el resultado de la combinación
    i = j = 0  #Estos son los indices que recorren las dos mitades

    #Se compara elementos de las dos mitades para combinarlos en orden
    while i < len(left) and j < len(right):
        if left[i] < right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1

    #Se agrega los elementos restantes de la mitad izquierda si aun quedan
    result.extend(left[i:])

    #Se agrega los elementos restantes de la mitad derecha si aun quedan
    result.extend(right[j:])

    #Retorna la lista combinada y ordenada
    return result

# Medir el tiempo de ejecución del mergesort
start_time = time.time()
sorted_list = mergesort(random_list.copy())  # Usar una copia de la lista para no modificar la original
end_time = time.time()

#Se muestra la lista organizada y el tiempo de ejecución
print("Organized list\n\n")
print(sorted_list)
print(f"Execution time: {end_time - start_time} seconds")

#Perfil del rendimiento del mergesort
cProfile.run('mergesort(random_list.copy())')
