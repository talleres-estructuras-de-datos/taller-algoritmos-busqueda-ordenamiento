import numpy as np

# Generar una lista aleatoria ordenada de 2000 elementos
random_list = sorted(list(np.random.randint(low=0, high=10000, size=2000)))
# Imprime la lista generada
print("Lista ordenada:", random_list)

def binary_search(arr, x):
    initial = 0
    final = len(arr) - 1
    iterations = 0

    while initial <= final:
        iterations += 1
        mid = (initial + final) // 2
        if arr[mid] == x:
            print(f"Total iterations: {iterations}")
            return mid
        # Si el valor del medio es menor al dato solicitado, el valor de inicio aumenta en uno
        elif arr[mid] < x:
            #Aumenta a la derecha el valor inicial
            initial = mid + 1
            print(f"Now the initial value is at the index: {initial}")
        else:
            #El valor final se reduce en uno, por lo que reduce el rango final a la izquierda
            final = mid - 1

    print(f"Total de iteraciones: {iterations}")
    return -1

# Solicitar al usuario el valor a buscar
search_value = int(input("Please enter the value that you want to find in the array: "))

response = binary_search(random_list, search_value)

if response != -1:
    print(f"The value {search_value} is located at {response}.")
else:
    print(f"The value {search_value} doesn't exist into the list.")
