import numpy as np
import time
import cProfile

# Generar una lista aleatoria de 2000 elementos
random_list = list(np.random.randint(low=0, high=20000, size=2000))

# Imprime la lista generada
print(f"This is the list without organization:\n\n {random_list}\n\n")

def quicksort(arr):
    #Si la lista tiene 0 o 1 elementos ya esta ordenada
    if len(arr) <= 1:
        return arr
    
    # El pivote viene a ser el elemento de la mitad de la lista 
    pivot = arr[len(arr) // 2]

    # Se divide la lista en tres sublistas:
    # left tiene los elementos menores que el pivote
    # middle los elementos iguales al pivote
    # right a  los elementos mayores que el pivote
    left = [x for x in arr if x < pivot]
    middle = [x for x in arr if x == pivot]
    right = [x for x in arr if x > pivot]

    # Ordenar recursivamente las sublistas y combinar
    return quicksort(left) + middle + quicksort(right)

# Se mide el tiempo de ejecución del quicksort
start_time = time.time()
sorted_list = quicksort(random_list.copy())  # Usar una copia de la lista para no modificar la original
end_time = time.time()

# Se muestra la lista organizada y el tiempo de ejecución
print("Organized list\n\n")
print(sorted_list)
print(f"Execution time: {end_time - start_time} seconds")

# Perfil del rendimiento del quicksort
cProfile.run('quicksort(random_list.copy())')
